import os
import csv
import re
import sys
import codecs
import json
import urllib.request, urllib.parse, urllib.error
from time import time as curr_time

from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement, tostring
from xml.dom import minidom

#import scapy
from scapy.all import rdpcap, wrpcap
from scapy.all import srp,Ether,IP,ARP,conf
#from scapy.all import *

##############################################################################
# Exported classes and functions
##############################################################################
#__all__ = ['find_file_type', 'find_csv', 'find_pcap', 'find_pcapng',
#           'find_xml_files', 'find_pcap_from_xml',
#           'find_simple_category', 'make_clean_file_name_string', 'pretty_print_xml',
#           'get_expected_chars', 'strike_info_from_xml', 'write_output_files']
#
#__attack_pcap_lib = 1

# find file with given type in the given path
def find_file_type(file_path, file_type):
  file_name = None
  # open path and search
  for currfile in os.listdir(file_path):
    if currfile.endswith("." + file_type):
      if file_name == None:
        file_name = currfile
        print(("Found " + file_type + " file named: " + currfile))
      else:
        print(("Warning: found additional " + file_type + " file named: " + currfile + ", but ignoring it."))
  if file_name == None:
    return None
  return file_path + file_name

# find csv in the given path
def find_csv(file_path):
  return find_file_type(file_path, "csv")

# find pcap in the given path
def find_pcap(file_path):
  return find_file_type(file_path, "pcap")

# find pcapng in the given path
def find_pcapng(file_path):
  return find_file_type(file_path, "pcapng")

def find_xml_files(file_path):
  xmlfile_names = []
  # open path and search
  for currfile in os.listdir(file_path):
    if currfile.endswith(".xml"):
      xmlfile_names.append( file_path + currfile)
  return xmlfile_names

def find_pcap_files(file_path):
  pcapfile_names = []
  # open path and search
  for currfile in os.listdir(file_path):
    if currfile.endswith(".pcap"):
      pcapfile_names.append( file_path + currfile)
  return pcapfile_names

def find_pcap_from_xml(xmlfile_name):
  pcapfile_name = xmlfile_name[:-4] + ".pcap"
  #TODO: also check the xml contents, names may not always match.
  if not os.path.isfile(pcapfile_name):
    print("WARNING: could not find pcap file for this XML, searching for other pcap files in this dir.")
    xml_dir = "/".join(xmlfile_name.split('/')[:-1]) + '/' #get the dir of this file
    pcapfile_name = parser.find_pcap(xml_dir)
  return pcapfile_name

def find_simple_category(category_string):
  category = category_string
  #TODO subcategories?
  if category[0:18] == "Denial of Service:":
    category = "DoS"
  elif category[0:28] == "/strikes/exploits/webapp/dos":
    category = "DoS"
  elif category[0:9] == "Exploits:":
    category = "Exploit"
  elif category[0:8] == "Malware:":
    category = "Malware"
  elif category[0:30] == "/strikes/malware/package_00000":
    category = "Malware"
  elif category[0:6] == "Worms:":
    category = "Malware"
  elif category[0:10] == "Backdoors:":
    category = "Malware"
  elif category[0:10] == "Shellcode:": #only in "malware" set?
    category = "Malware"
  elif category[0:8] == "Fuzzers:": #only in "malware" set?
    category = "Malware"
  elif category[0:15] == "Reconnaissance:":
    category = "Recon"
  elif category[0:8] == "Generic:":
    category = "Generic"
  return category

def make_clean_file_name_string(old_string):
  #leaving all of these replaces separate for now, for easier debugging
  new_string = old_string.replace(' ', "_").replace('+', "_").replace('-', "_")
  new_string = new_string.replace('%21','_').replace('%22','_').replace('%24','_').replace('%25','_').replace('%26','_').replace('%27','_').replace('%28','_').replace('%29','_')
  new_string = new_string.replace('%2F','_').replace('%2C','_').replace('%2B','_')
  new_string = new_string.replace('%3A','_').replace('%3C','_').replace('%3D','_').replace('%3E','_')
  new_string = new_string.replace('%40','_').replace('%5B','_').replace('%5C','_').replace('%5D','_').replace('%7E','_')
  return new_string

# See https://pymotw.com/2/xml/etree/ElementTree/create.html
#TODO: can sometimes leave etxra empty lines in output.  see note in vlan_filter.py
def pretty_print_xml(elem):
  """Return a pretty-printed XML string for the Element.
  """
  rough_string = ElementTree.tostring(elem, 'utf-8')
  #rough_string = filter(lambda x: not re.match(r'^\s*$', x), rough_string) #pre-process, for consistency
  reparsed = minidom.parseString(rough_string)
  pretty = reparsed.toprettyxml(indent="  ")
  #return filter(lambda x: not re.match(r'^\s*$', x), pretty) #'pretty' string can have empty lines sometimes.
  return pretty

def get_expected_chars():
  chars_used = set(['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'])
  chars_used = chars_used.union(set(['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']))
  chars_used = chars_used.union(set(['1','2','3','4','5','6','7','8','9','0','_','.','[',']']))
  return chars_used

def strike_info_from_xml(xml_file):
  tree = ElementTree.parse(xml_file)
  root = tree.getroot()

  pcap_elems = root.findall('pcap')

  strike_info = {}
  #strike_info["Strike Name"] = None

  # get the category
  category_elems = root.findall('pcap/filters/filter/category')
  if len(category_elems) == 0:
    print(("WARNING: found no category element in file " + xml_file))
    category = "unknown"
  elif len(category_elems) > 1:
    print(("WARNING: found more than one category element in file " + xml_file))
    category = category_elems[0].attrib.get("value")
    #TODO: it is now valid to have more than one filter, which each may contain one
  else:
    category = category_elems[0].attrib.get("value")
  strike_info["Category"] = category

  if len(category_elems) > 0:
    categories = category_elems[0].attrib
    del categories["value"]
    strike_info["Categories"] = categories

  # get the label
  label_elems = root.findall('pcap/filters/filter/label')
  if len(label_elems) == 0:
    print(("WARNING: found no label element in file " + xml_file))
    label = "unknown"
  elif len(label_elems) > 1:
    print(("WARNING: found more than one label element in file " + xml_file))
    label = label_elems[0].attrib.get("value")
    #TODO: it is now valid to have more than one filter, which each may contain one
  else:
    label = label_elems[0].attrib.get("value")
  strike_info["Strike Name"] = label

  # get the pcap file name
  # TODO

  # get the source
  # TODO
  strike_info["Data Source"] = "unknown"

  # get the filter info
  filter_elems = root.findall('pcap/filters/filter')
  if len(filter_elems) == 0:
    print(("ERROR: found no filter element in file " + xml_file))
  elif len(filter_elems) > 1:
    print(("WARNING: found more than one filter element in file " + xml_file))
    #TODO: it is now valid to have more than one filter

  sourceIP = filter_elems[0].attrib.get("sourceIP")
  strike_info["Source IP"] = sourceIP

  destIP = filter_elems[0].attrib.get("destIP")
  strike_info["Destination IP"] = destIP

  sourcePort = filter_elems[0].attrib.get("sourcePort")
  strike_info["Source Port"] = sourcePort

  destPort = filter_elems[0].attrib.get("destPort")
  strike_info["Destination Port"] = destPort

  protocol = filter_elems[0].attrib.get("protocol")
  strike_info["Protocol"] = protocol

  #print strike_info

  return strike_info

# write_output_files (needs: strike_info (map), matching packets (plist), path, filename)
def write_output_files (strike_info, packet_list, path, filename):
  xmlOutFile = open(path + filename + ".xml", 'w')
  #pcapOutFile = open(path + filename + ".pcap", 'w')

  packet_count = len(packet_list)

  # generate the xml object that you would want to output
  root = Element('pcapData')
  pcapElement = SubElement(root, "pcap")
  pcapElement.set("file", filename + ".pcap")

  filters = SubElement(pcapElement, "filters")

  filterElem = SubElement(filters, "filter")

  label = SubElement(filterElem, "label")
  label.set("value", strike_info["Strike Name"])

  #keep the currently-selected category label
  category = SubElement(filterElem, "category")
  category.set("value", find_simple_category(strike_info["Category"]))

  #also keep the other category labels
  if "Categories" in strike_info:
    for cat in strike_info["Categories"]:
      category.set(cat, strike_info["Categories"][cat])

  #payload = SubElement(filters, "payload")
  #payload.set("value", "none")

  traffic_info = SubElement(filterElem, "traffic")
  traffic_info.set("packetCount", str(packet_count))
  #TODO add byte count?  latency/jitter/etc?

  filterElem.set("sourceIP", strike_info["Source IP"])
  filterElem.set("destIP", strike_info["Destination IP"])
  if strike_info["Source Port"]:
    filterElem.set("sourcePort", strike_info["Source Port"])
  if strike_info["Destination Port"]:
    filterElem.set("destPort", strike_info["Destination Port"])
  if strike_info["Protocol"]:
    filterElem.set("protocol", strike_info["Protocol"])

  #TODO: should these go in the filters line with the ips, etc.??  Ask brian.
  filterElem.set("startTime", str(packet_list[0].time))
  filterElem.set("endTime", str(packet_list[packet_count-1].time))
  filterElem.set("duration", str(packet_list[packet_count-1].time - packet_list[0].time))
  #time = SubElement(filters, "time")
  #time.set("startTime", str(packet_list[0].time))
  #time.set("endTime", str(packet_list[packet_count-1].time))
  #time.set("duration", str(packet_list[packet_count-1].time - packet_list[0].time))

  success = SubElement(filterElem, "success")
  if "success" in strike_info:
    success.set("value", strike_info["success"])
  else: #assume true if not specified (this is reasonably common)
    success.set("value", "True")

  if strike_info["NCD"]: #omit if not specified (very common)
    incident = SubElement(filterElem, "incident")
    incident.set("NCD", "True")

  if strike_info["CER"]: #omit if not specified (very common)
    event = SubElement(filterElem, "event")
    event.set("CER", "True")

  ##now in the label field, see below.
  ##attack_name = SubElement(pcapElement, "attackName")
  ##attack_name.text = strike_info["Strike Name"]
  #TODO: consider re-adding one/both of these at some point
  #refs = SubElement(pcapElement, "attackReferences")
  #strike_ref = SubElement(refs, "reference")
  #strike_ref.text = strike_info["Strike Reference URL"]
  #info_ref = SubElement(refs, "reference")
  #info_ref.text = strike_info["Strike Info"]

  meta = SubElement(filterElem, "metadata")
  meta.set("created", str(curr_time()))
  meta.set("source", strike_info["Data Source"])
  if "Source File" in strike_info: #also common to omit this
    meta.set("sourceFile", strike_info["Source File"])
  else:
    meta.set("sourceFile", filename + ".pcap") #TODO: might want to have the merge script add this instead, not sure if it matters.

  # write xml
  #print strike_info
  #print root
  #print pretty_print_xml(root)
  xmlOutFile.write(pretty_print_xml(root))

  # write pcap
  wrpcap(path + filename + ".pcap", packet_list)

#used in tests
def remove_all_files(path):
  xmlfile_names = find_xml_files(path)
  for xmlfile_name in xmlfile_names:
    os.remove(xmlfile_name)
  pcapfile_names = find_pcap_files(path)
  for pcapfile_name in pcapfile_names:
    os.remove(pcapfile_name)
