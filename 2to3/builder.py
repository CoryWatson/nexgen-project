#!/usr/bin/env python3

import warnings
import util
from time import gmtime, strftime
import pandas
import os
import sys
import argparse
from scapy.all import *
import multiprocessing as mp
import cProfile

warnings.filterwarnings('ignore')

# Global Vars #
nowTime = strftime("%Y-%m-%d_%H_%M_%S", gmtime())
output = mp.Queue()


class ModifiedParser(argparse.ArgumentParser):
    """
    Overrides argparse's ArgumentParser in order to write both the error
    message and the help message out to stderr before exiting.
    """

    def error(self, message):
        sys.stderr.write('error:%s\n' % message)
        self.print_help(sys.stderr)
        sys.exit(1)


class Counter(object):
    """
    Counter is commented out and unused in this file.
    """

    def __init__(self, initval=0):
        self.val = mp.RawValue('i', initval)
        self.lock = mp.Lock()

    def increment(self):
        with self.lock:
            self.val.value += 1

    @property
    def value(self):
        return self.val.value


# Note: this is unrelated to the fullFilenameStripper() in makexml
def filenameStripper(filename):
    """
    Takes the filename and breaks it down, stripping the suffix's and
    prefix's leaving only the core.
    """

    print(filename)
    splitFile = filename.split(os.sep)
    print(splitFile)
    print((splitFile[-1]))
    source = splitFile[-1].split(".pcap")[0]
    print(source)
    return source


def parse_source_netflow(inFile):
    """
    Expects argus netflow csv as file import, converts to dataframes and
    filters for Botnet associated traffic. Drops flows with One or Fewer
    packets, and also finds the service port and drops unneeded columns, and
    removes duplicate rows.
    """
    print("Reading file into dataframes:")
    df = util.parse_argus_csv_file(inFile)

    df.TotPkts = pandas.to_numeric(df.TotPkts, errors='coerce').fillna(0)
    df = df[df.TotPkts > 4]

    df.Sport = pandas.to_numeric(df.Sport, errors='coerce').fillna(0)
    df.Dport = pandas.to_numeric(df.Dport, errors='coerce').fillna(0)

    df = df.rename(columns={'Sport': 'SvcPort'})
    df['SvcPort'] = df['SvcPort'].mask(df['SvcPort'] > df["Dport"], df["Dport"])

    df_cols_to_drop = df.columns.tolist()
    df_cols_to_drop.remove('SvcPort')
    df_cols_to_drop.remove('Proto')
    df_cols_to_drop.remove('SrcAddr')
    df_cols_to_drop.remove('DstAddr')

    df = df.drop(columns=df_cols_to_drop)

    df = df.drop_duplicates()

    tcp_df = df[df.Proto == "tcp"]
    udp_df = df[df.Proto == "udp"]
    return tcp_df, udp_df


def write_pcap(filename, pkt):
    """
    Takes the packet data and writes the filename.
    """
    scapy.all.wrpcap(filename + ".pcap", pkt, append=True)
    # TODO don't really need this wrapper anymore?


def make_path():
    """
    Makes the TCP and UDP files in the output folder, so that when the
    file information is split the udp and tcp will be sent to the correct
    folder for evaulation.
    """

    try:
        os.makedirs("output")
    except OSError:
        if not os.path.isdir("output"):
            raise
    try:
        os.makedirs("output" + os.sep + "tcp")
    except OSError:
        if not os.path.isdir("output" + os.sep + "tcp"):
            raise
    try:
        os.makedirs("output" + os.sep + "udp")
    except OSError:
        if not os.path.isdir("output" + os.sep + "udp"):
            raise


def find_protos_slow(pcap, outDir):
    """
    Intakes argus netflow csv and pcap then cross-references to find Botnets
    Outputs many pcap flows which it believes belong to the argus file.
    """

    print("Reading Pcap...")
    all_packets = scapy.all.rdpcap(pcap)
    all_packets = all_packets.filter(lambda x: IP in x)
    print("Found IP Traffic. Splitting into protocols...")

    make_path()

    for pkt in all_packets:
        if 'UDP' in pkt:
            write_pcap(outDir + "udp_raw" + nowTime, pkt)
        elif 'TCP' in pkt:
            write_pcap(outDir + "tcp_raw" + nowTime, pkt)
    # remove pcap from memory
    all_packets = None
# TODO replace writepcap


def find_protos(pcap, outDir):
    """
    Intakes argus netflow csv and pcap then cross-references to find Botnets
    Outputs many pcap flows which it believes belong to the argus file.
    """

    filter_str = "tcp and ip"
    pcap_reader = PcapReader(tcpdump(pcap, args=["-w", "-", filter_str], getfd=True))
    write_pcap(outDir+"tcp_raw"+nowTime, pcap_reader)

    filter_str = "udp and ip"
    pcap_reader = PcapReader(tcpdump(pcap, args=["-w", "-", filter_str], getfd=True))
    write_pcap(outDir+"udp_raw"+nowTime, pcap_reader)


def parse_attacks(pcap, netflow, outDir):
    """
    Looping thru the binetflow.txt file and finds the packets that find each
    flow and seperates them into each packet for that flow.
    """

    print("Reading Netflow...")
    tcp_df, udp_df = parse_source_netflow(netflow)
    print("Parsing...")
    source = filenameStripper(pcap)

    tcp_packets = rdpcap(outDir + "tcp_raw" + nowTime + ".pcap")
    for i, row in tcp_df.iterrows():
        y = (tcp_packets.filter(lambda x:
        (x[IP].src == row.SrcAddr and x[IP].dst == row.DstAddr and (x[IP].sport == row.SvcPort or x[IP].dport == row.SvcPort))
        or
        (x[IP].dst == row.SrcAddr and x[IP].src == row.DstAddr and (x[IP].dport == row.SvcPort or x[IP].sport == row.SvcPort))
        ))
        if(y):
            newFileName = "tcp" + os.sep + "[CTU][Malware]" + str(row.SvcPort) + "_" + row.SrcAddr + "_" + row.DstAddr + "_" + source
            write_pcap(outDir + newFileName, y)

    # remove pcap from memory
    tcp_packets = None

    udp_packets = rdpcap(outDir + "udp_raw" + nowTime + ".pcap")
    for i, row in udp_df.iterrows():
        y = (udp_packets.filter(lambda x:
        (x[IP].src == row.SrcAddr and x[IP].dst == row.DstAddr and (x[IP].sport == row.SvcPort or x[IP].dport == row.SvcPort))
        or
        (x[IP].dst == row.SrcAddr and x[IP].src == row.DstAddr and (x[IP].dport == row.SvcPort or x[IP].sport == row.SvcPort))
        ))
        if(y):
            newFileName = "udp" + os.sep + "[CTU][Malware]" + str(row.SvcPort)
            + "_" + row.SrcAddr + "_" + row.DstAddr + "_" + source
            write_pcap(outDir + newFileName, y)
    udp_packets = None


def find_attacks(pcap, netflow, outDir):
    """
    Wrapper function that finds the protos and prints the protos.
    """

    find_protos(pcap, outDir)
    print("Reading TCP and UDP captures into memory")
    print("Creating TCP and UDP Attack Files")
    parse_attacks(pcap, netflow, outDir)


if __name__ == '__main__':
    # Set up the argparser
    argparser = ModifiedParser(description='Split packet capture into '
                               'individual files based on a netflow')
    required = argparser.add_argument_group("required arguments")
    required.add_argument("-i", "--input", dest="inPath", nargs='?',
                          default="pcap" + os.sep,  help="Input directory",
                          required=True)
    required.add_argument("-o", "--output", dest="outPath", nargs='?',
                          default="output" + os.sep, help="Output directory",
                          required=True)
    required.add_argument("-n", "--netflow", dest="netflow", nargs='?',
                          default="netflow" + os.sep,
                          help="Netflow for comparison", required=True)
    # Parse & validate the arguments - inPath
    args = argparser.parse_args()

    netflow = args.netflow
    # TODO: validate the netflow arg
    # TODO: find list of individual netflows in netflow dir

    # Parse & validate the arguments - inPath
    in_path = args.inPath
    # if in_path[-1] != os.sep:
    #     in_path += os.sep

    # if not os.path.exists(in_path):
    #     print("Error: input path not found!")
    #     sys.exit()

    # Parse & validate the arguments - outPath
    out_path = args.outPath
    if out_path[-1] != os.sep:
        out_path += os.sep

    if out_path == in_path:
        print("Error: output path must be different from input path!")
        sys.exit()

    if not os.path.exists(out_path):
        os.makedirs(out_path)

    # TODO: match individual netflow files with the pcap files before invoking
    # find_attacks
    cProfile.run("find_attacks(in_path, netflow, out_path)", sort="time")
