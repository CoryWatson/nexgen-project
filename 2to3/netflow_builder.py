#!/usr/bin/env python3

# Oak Ridge National Laboratory
#
# File: netflow_builder.py
#
# Author: Kevin Spakes
# Date:   5/21/2019
#
# Takes a give PCAP file and converts it to an argus binary. Then, it converts
# the binary to a plain text netflow file.
#
# IMPORTANT: For the correct output format, the ra.conf file needs to be in the
# same directory as this script.

import os
import argparse


# Displays usage of program
def usage():
    print("NAME\n"
          "    Netflow Builder \n"
          "\nDESCRIPTION\n"
          "    Creates an argus binary file and a netflow text file that can "
          "be used with the builder.py program.\n"
          "\n    Specify the pcap file that is to be used. If the pcap file "
          "is in another location than here, specify "
          "\n    the complete path to the pcap.\n"
          "\nSYNOPSIS\n"
          "      Usage: python3 netflow_builder.py <PCAP File>\n"
          "      Example: python3 netflow_builder.py test.pcap")


# Checks if the file has a '.pcap' extension
def extensionCheck(pcap):
    testPcap = pcap[-4:]
    if (testPcap == "pcap" or testPcap == "PCAP"):
        return True
    return False


# Check if the testdata/netflow and testdata/argus-binary directories exist
# If they do not exist then create them.
def directoryCheck():
    if not (os.path.exists("testdata")):
        os.mkdir("testdata")
        print("Created testdata directory")

    if not (os.path.exists("testdata" + os.sep + "netflow")):
        os.mkdir("testdata" + os.sep + "netflow")
        print(("Created testdata" + os.sep + "netflow directory"))

    if not (os.path.exists("testdata" + os.sep + "argus-binary")):
        os.mkdir("testdata" + os.sep + "argus-binary")
        print(("Created testdata" + os.sep + "argus-binary directory"))


if __name__ == "__main__":
    try:
        # Parse arguments for command line
        parser = argparse.ArgumentParser(description="Netflow Builder")
        required = parser.add_argument_group("required arguments")
        required.add_argument("-p", "--pcap", dest="pcap", nargs="?",
                              default="", help="Specify the pcap name and "
                              "location", required=True)
        args = parser.parse_args()

        # Local variables
        argus = args.pcap[:-4] + "argus"
        text = args.pcap[:-4] + "txt"
        argusFile = argus.split(os.sep)[-1]
        textFile = text.split(os.sep)[-1]

        # Raise error if the specified file does not exist
        if not (os.path.isfile(args.pcap)):
            raise FileNotFoundError

        # Run directoryCheck
        directoryCheck()

        # If the given file is not a pcap file then display error
        if (extensionCheck(args.pcap) is False):
            # Error to be displayed
            print("The file you specified is not in the corrcet format.\n"
                  "The file must have a '.pcap' file extension.")

        # Create the netflow
        else:
            # Takes the pcap file and creates an argus file
            os.system("argus -r " + args.pcap + " -w testdata" + os.sep
                      + "argus-binary" + os.sep + argusFile)

            # Display current status
            print((argusFile + " created"))

            # Takes the argus output and creates a text file
            os.system("ra -F ra.conf -r testdata" + os.sep + "argus-binary"
                      + os.sep + argusFile + " > testdata" + os.sep + "netflow"
                      + os.sep + textFile)

            # Display final status
            print((textFile + " created"))

    # If no PCAP file was specified then display error with usage
    except IndexError:
        print("You did not specify a PCAP file to be processed\n")
        usage()

    # If the file that was specified does not exist, display error message
    except FileNotFoundError:
        print("The file you specified was not found")

    # Print generic error message for any unhandled errors
    except Exception as e:
        print((type(e), e))
        print("\nYeah, I don't know what you just did, but it wasn't good...\n"
              "If you could go back and check to make sure you did everything"
              "\ncorrectly, that'd be great!")
