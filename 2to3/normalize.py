#Common functions for normalizing data and etc.

import os
import csv
import re
import sys
#import urllib.request, urllib.parse, urllib.error
import argparse
import datetime
import functools
import warnings
from functools import partial
from pprint import pprint

import pandas as pd
from pandas import DataFrame, read_csv

import numpy as np
import matplotlib.pyplot as plt

from scipy.stats import entropy as H

from sklearn.preprocessing import FunctionTransformer
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.metrics import confusion_matrix

def log_normalize_col(df, col):
    df[col+"_norm"] = np.log1p(df[col])

    if df[col].isnull().sum() > 0 or df[col+"_norm"].isnull().sum() > 0:
        print((df[col]))
        print((df[col+"_norm"]))
        raise ValueError('Unexpected NaN found in log_normalize_col in col ' + col)

    df[col] = df[col+"_norm"]
    del df[col+"_norm"]

def bin_col(df, col):
    col_min = min(df[col].tolist())
    col_max = max(df[col].tolist())
    num_buckets = 10
    bucket_size = (1.0*(col_max - col_min)) / (1.0*num_buckets)
    print(("bucket size", bucket_size))

    i=col_min
    buckets = [] #TODO: the way this is populated is kind of hacky.
    if col_min == 0.0:
        buckets.append(-0.000000001)
        buckets.append(0.000000001)
        i += bucket_size
    else:
        buckets.append(col_min)
        i += bucket_size
    while i < col_max:
        buckets.append(i)
        i += bucket_size
    buckets.append(i)

    #don't really care what the label the bucket has, so just using its lower bound.
    df[col+"_bin"] = pd.cut(df[col], bins=buckets, labels=buckets[:-1], include_lowest=True)

    if df[col].isnull().sum() > 0 or df[col+"_bin"].isnull().sum() > 0:
        print((df[col]))
        print((df[col+"_bin"]))
        raise ValueError('Unexpected NaN found in bin_col in col ' + col)

    df[col] = df[col+"_bin"]
    del df[col+"_bin"]

    #print df[col]

# def bin_col(df, col):
#     col_min = min(df[col].tolist())
#     col_max = max(df[col].tolist())
#     num_buckets = 10
#     zero_bucket = True
#
#     print "binning col", col
#     print "min", col_min, "max", col_max
#     bucket_size = (1.0*(col_max - col_min)) / (1.0*num_buckets) #TODO: case where zero_bucket == false
#     print "found bucket size", bucket_size
#
#     def get_bucket(value, col_min, col_max, bucket_size, zero_bucket):
#         if zero_bucket and value == 0:
#             return 'z'
#         index = int( (1.0*(value - col_min)) / (1.0*bucket_size) )
#         print "value", value, "diff", (1.0*(value - col_min)),  "found index", index
#         return chr( ord('a') + index)
#
#     get_bucket_partial = functools.partial(get_bucket, col_min, col_max, bucket_size, zero_bucket)
#
#     df[col+"_bin"] = df[col].apply(get_bucket_partial)
#     df[col] = df[col+"_bin"]
#     del df[col+"_bin"]

def find_map_of_top_by_rank(df, col, top=10):
    counts = {}
    for x in df[col]:
        if x in counts:
            counts[x] += 1
        else:
            counts[x] = 1
    ranked = sorted(list(counts.items()), reverse=True, key=lambda tup: tup[1])
    ranked = ranked[:top]
    rank_map = {}
    for index, item in enumerate(ranked):
        rank_map[item[0]] = index
    return rank_map

def encode_with_map(val, val_map, default):
    if val in val_map:
        return val_map[val]
    else:
        return default

def encode_with_ranges(val, val_ranges, default):
    for r in val_ranges:
        if r["min"] <= val and r["max"] >= val:
            return r["result"]
    return default

def normalize_by_rank(df, col):
    rank_map = find_map_of_top_by_rank(df, col, top=5)
    df[col+"_norm"] = df.apply (lambda row: encode_with_map (row[col], rank_map, 5),axis=1)

    df[col] = df[col+"_norm"]
    del df[col+"_norm"]

def encode_CTU_labels(label):
    #Background
    if re.match("flow=Background", label):
        return "Background"
    if re.match("flow=From-Background", label):
        return "Background"
    if re.match("flow=To-Background", label):
        return "Background"
    #Botnet
    if re.match("flow=Botnet", label):
        return "Botnet"
    if re.match("flow=From-Botnet", label):
        return "Botnet"
    if re.match("flow=To-Botnet", label):
        return "Botnet"
    #Normal
    if re.match("flow=Normal", label):
        return "Normal"
    if re.match("flow=From-Normal", label):
        return "Normal"
    if re.match("flow=To-Normal", label):
        return "Normal"
    #Other???
    print(("Label value did not match known patterns: " + label))
    return "Other"

def normalize_CTU_labels(df, col):
    df[col+"_norm"] = df.apply (lambda row: encode_CTU_labels (row[col]),axis=1)

    df[col] = df[col+"_norm"]
    del df[col+"_norm"]

def encode_port(port):
    #encoding ports as follows, in order of "normal-ness":
    # 0 - 80 & 443
    # 1 - ports 1 to 1024 (other than above) (note 1024 is supposedly unused.)
    # 2 - ports 1025 to 49151 ("registered ports")
    # 3 - ports 49152 to 65535 ("ephemeral ports", at least on modern OSes.)
    # 4 - "none" or "other" ports (eg. ICMP)
    port_ranges = []
    port_ranges.append({"min":80, "max":80, "result":0})
    port_ranges.append({"min":443, "max":443, "result":0})
    port_ranges.append({"min":1, "max":1024, "result":1})
    port_ranges.append({"min":1025, "max":49151, "result":2})
    port_ranges.append({"min":49152, "max":65535, "result":3})
    try:
        return encode_with_ranges(int(port), port_ranges, 4)
    except ValueError:
        return encode_with_ranges(-1, port_ranges, 4)

def normalize_ports(df, col):
    df[col+"_norm"] = df.apply (lambda row: encode_port (row[col]),axis=1)

    df[col] = df[col+"_norm"]
    del df[col+"_norm"]

def encode_country(country):
    #encoding countries as follows, in order of "locality":
    # 0 - ZZ (presumably non-routable, meaning local)
    # 1 - US
    # 2 - blank or other (this is maybe 10% or so, at least on the 6012 dataset)
    country_map = {}
    country_map["ZZ"] = 0
    country_map["US"] = 1
    return encode_with_map(country, country_map, 2)

def normalize_country(df, col):
    df[col+"_norm"] = df.apply (lambda row: encode_country (row[col]),axis=1)

    df[col] = df[col+"_norm"]
    del df[col+"_norm"]

def encode_proto(proto):
    #ordering by "more organized"; lower should have longer durations and more patterns within the flow.
    # 0 - tcp
    # 1 - udp
    # 2 - other (icmp, L2 mgmt protos, L3 multicast protos, ...)
    proto_map = {}
    proto_map["tcp"] = 0
    proto_map["udp"] = 1
    return encode_with_map(proto, proto_map, 2)

def normalize_proto(df, col):
    df[col+"_norm"] = df.apply (lambda row: encode_proto (row[col]),axis=1)

    df[col] = df[col+"_norm"]
    del df[col+"_norm"]

def encode_proto_categorical(proto):

    #classes in dataset were:
    #"tcp", "udp", "icmp", "ipv6-icmp", "arp", "igmp", "llc", "loop", "pim", "well", "decrc"
    #keeping tcp, udp, arp,
    # grouping icmp w ipv6-icmp
    # now grouping layer 2 traffic together  (arp, loop, llc, well, decrc)
    # vs L3 multicast (pim, igmp)
    # vs "other" (which is not present in the 10k sample)
    proto_map = {}
    proto_map["tcp"] = "tcp"
    proto_map["udp"] = "udp"
    proto_map["icmp"] = "icmp"
    proto_map["ipv6-icmp"] = "icmp"
    proto_map["arp"] = "L2"
    proto_map["loop"] = "L2"
    proto_map["llc"] = "L2"
    proto_map["well"] = "L2"
    proto_map["decrc"] = "L2"
    proto_map["pim"] = "multicast"
    proto_map["igmp"] = "multicast"
    return encode_with_map(proto, proto_map, "other")

def normalize_proto_categorical(df, col):
    df[col+"_norm"] = df.apply (lambda row: encode_proto_categorical (row[col]),axis=1)

    df[col] = df[col+"_norm"]
    del df[col+"_norm"]

def encode_tcp_opt(opt):
    # For now, only care if tcp options field is blank or not (and note blank is "falsy")
    if not opt:
        return 0
    else:
        return 1

def normalize_tcp_opt(df, col):
    df[col+"_norm"] = df.apply (lambda row: encode_tcp_opt (row[col]),axis=1)

    df[col] = df[col+"_norm"]
    del df[col+"_norm"]

def handle_argus_junk_fields(flow_frame):
    #TODO: possibly revisit these later
    #Dropping "odd" fields
    cols = flow_frame.columns.values
    if "State" in cols:
        flow_frame = flow_frame.drop(["State"], axis=1)
    if "Dir" in cols:
        flow_frame = flow_frame.drop(["Dir"], axis=1)
    if "Inode" in cols:
        flow_frame = flow_frame.drop(["Inode"], axis=1)
    if "Flgs" in cols:
        flow_frame = flow_frame.drop(["Flgs"], axis=1)
    if "CCDetector" in cols:
        flow_frame = flow_frame.drop(["CCDetector"], axis=1)
    #doing some misc cleanup.
    #flow_frame.fillna(value={'SrcWin': 0, 'DstWin': 0}, inplace=True)
    flow_frame.fillna(value=0, inplace=True) #TODO: confirm that this is *always* ok to do.
    flow_frame = flow_frame.loc[:,flow_frame.apply(pd.Series.nunique) != 1] #removes constant columns

    return flow_frame

#this also does the log-normalizing where needed.  TODO:consider renaming?
def create_ranked_features(flow_frame):
    flow_cols = flow_frame.columns.values.tolist()
    flow_count = len(flow_frame)

    for col_name in flow_cols:
        if col_name == "Sport" or col_name == "Dport":
            print(("Normalizing", col_name, "with special handling"))
            normalize_ports(flow_frame, col_name)
        elif col_name == "sCo" or col_name == "dCo":
            print(("Normalizing", col_name, "with special handling"))
            normalize_country(flow_frame, col_name)
        elif col_name == "Proto":
            print(("Normalizing", col_name, "with special handling"))
            normalize_proto(flow_frame, col_name)
        elif col_name == "TcpOpt":
            print(("Normalizing", col_name, "with special handling"))
            normalize_tcp_opt(flow_frame, col_name)
        elif col_name == "Label":
            print(("Normalizing", col_name, "with special handling"))
            normalize_CTU_labels(flow_frame, col_name)
        elif(flow_frame[col_name].dtype == np.float64 or flow_frame[col_name].dtype == np.int64):
            if pd.isna(min(flow_frame[col_name].tolist())) or pd.isna(max(flow_frame[col_name].tolist())):
                flow_frame[col_name].astype('category', copy=False)
                print(("Can't normalize", col_name, "min", min(flow_frame[col_name].tolist()), "max", max(flow_frame[col_name].tolist())))
            else:
                print(("Normalizing", col_name, "(min", min(flow_frame[col_name].tolist()), "max", max(flow_frame[col_name].tolist()), ")"))
                log_normalize_col(flow_frame, col_name)
                #bin_col(flow_frame, col_name)
                #flow_frame[col_name].astype('category', copy=False)
                #plot_feat_values(flow_frame, col_name)
        else:
            flow_frame[col_name].astype('category', copy=False)
            print(("Non-numeric:", col_name))
    return flow_frame

def create_categorical_features(flow_frame):
    flow_cols = flow_frame.columns.values
    for col_name in flow_cols:
        if(flow_frame[col_name].dtype == np.float64 or flow_frame[col_name].dtype == np.int64):
            if pd.isna(min(flow_frame[col_name].tolist())) or pd.isna(max(flow_frame[col_name].tolist())):
                flow_frame[col_name].astype('category', copy=False)
                print(("Can't normalize", col_name, "min", min(flow_frame[col_name].tolist()), "max", max(flow_frame[col_name].tolist())))
            else:
                print(("Normalizing", col_name, "min", min(flow_frame[col_name].tolist()), "max", max(flow_frame[col_name].tolist())))
                log_normalize_col(flow_frame, col_name)
                bin_col(flow_frame, col_name)
                flow_frame[col_name].astype('category', copy=False)
                #plot_feat_values(flow_frame, col_name)
        else:
            flow_frame[col_name].astype('category', copy=False)
            print(("Non-numeric:", col_name))
    return flow_frame
