import attack_pcap_lib
import util
import os
import re
import builder
import argparse
import pandas as pd
from shutil import copyfile
import xml.etree.cElementTree as et

class ModifiedParser(argparse.ArgumentParser):
    """
    Overrides argparse's ArgumentParser in order to write both the error message
    and the help message out to stderr before exiting.
    """
    def error(self, message):
        sys.stderr.write('error:%s\n' % message)
        self.print_help(sys.stderr)
        sys.exit(1)

def getvalueofnode(node):
    return node.txt if node is not None else None

def moveDf(df,folder):
    df = df_subset
    folder = "html"
    df.Row = df.Row.astype('string')
    df.Source = df.Source.astype('string')

def buildNetflowList():
    dflist = list()
    for i in range(1, 14):
        binetflows = [f for f in os.listdir(str(i)) if f[-9:] == "binetflow"][0]
        netflow = builder.parse_source_netflow(str(i)+"/"+binetflows)
        netflow['Folder'] = [i]*len(netflow)
        dflist.append(netflow)
    return dflist

def buildDataframe():
    print("Building Dataframe List from XMLs...")
    filenames = os.listdir("output/tcp/")
    pattern = '.*.xml'
    r = re.compile(pattern)
    o = list(filter(r.match, filenames))
    d = dict()
    dfcols = ['Row', 'Source', 'PacketCount', 'Duration', 'DestPort', 'SourcePort']
    df_xml = pd.DataFrame(columns=dfcols)
    for file in o:
        parsed_xml = et.parse("output/tcp/"+file)
        node = parsed_xml.getroot()
        row = file.split('_')[-1].split('.')[0]
        source = file.split('.')[0]
        packetCount = node[0][0][0][2].get('packetCount')
        duration = node[0][0][0].get('duration')
        destPort = node[0][0][0].get('destPort')
        sourcePort = node[0][0][0].get('sourcePort')


        df_xml = df_xml.append(
            pd.Series([row, source, packetCount, duration,
            destPort,sourcePort], index=dfcols), ignore_index=True)
    return df_xml
outPath = "bgp/"
port = 4506
def parseDataframe(df_xml,port,outPath):
    print(("Parsing & Filtering pcaps into Folder: " + outPath))
    df_xml.Duration = df_xml.Duration.astype('float')
    df_xml.PacketCount = df_xml.PacketCount.astype('int64')
    df_xml.DestPort = df_xml.DestPort.astype('int64')
    df_subset = df_xml.query("(PacketCount > 4) & (Duration > 2) & (Duration < 120)")
    df_subset = df_subset.query("(DestPort == 179)")
    df_subset
    name_of_df_values = [x[1] for x in df_subset.values]
    outdir = "output/tcp/"
    for f in name_of_df_values:
        for ext in ['.pcap','.xml']:
            copyfile(outdir+f+ext,outdir+outPath+f+ext)

#filtered_xml = df_xml[pos_mask].reset_index()
#filtered_xml

if __name__ == '__main__':
    # Set up the argparser
    argparser = ModifiedParser()
    argparser.add_argument("port", nargs='?', default=80, help="Port to match on")
    argparser.add_argument("outPath", nargs='?', default="http/", help="Output pcaps to folder")
    argparser.add_argument("netflow", nargs='?', default="netflow/", help="Netflow for Comparison")

    args = argparser.parse_args()
    port = args.port
    outPath = args.outPath

    if outPath[-1] != '/':
        outPath += '/'

    if not os.path.exists(outPath):
        os.makedirs(outPath)

    df_xml = buildDataframe()

    parseDataframe(df_xml, 80, "http/")
    parseDataFrame(df_xml, 25, "smtp/")
    parseDataframe(df_xml, 22, "ssh/")
    parseDataFrame(df_xml, 443, "https/")
    parseDataframe(df_xml, 3389, "rdp/")
    parseDataFrame(df_xml, 65500, "random/")
    parseDataFrame(df_xml, 6667, "irc/")
    parseDataframe(df_xml, 3128, "proxy/")
    parseDataFrame(df_xml, 888, "accessbuilder/")
    parseDataframe(df_xml, 2869, "connectionsharing/")
    parseDataFrame(df_xml, 4506, "saltmaster/")
    parseDataframe(df_xml, 81, "81/")
    parseDataFrame(df_xml, 82, "82/")
    parseDataframe(df_xml, 88, "88/")
    parseDataFrame(df_xml, 129, "129/")
    parseDataframe(df_xml, 999, "999/")
    parseDataFrame(df_xml, 8080, "8080/")
    parseDataframe(df_xml, 135, "135/")
