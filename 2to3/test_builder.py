import unittest
import builder
import os


class TestBuilder(unittest.TestCase):
    def test_filenameStripper(self):
        file_name={
        # ".."+os.sep+".."+os.sep+"temp"+os.sep+"[CTU][Malware]rbot_1.pcap",
        # os.sep+"temp"+os.sep+"[CTU][Malware]rbot_1.pcap",
        # os.sep+"[CTU][Malware]rbot_1.pcap",
        # "[CTU][Malware]rbot_1.pcap"
        "."+os.sep+"pcap"+os.sep+"capture_win9.pcap",
        "."+os.sep+"pcap"+os.sep+"capture_win9.pcap"
        }

        for i in file_name:
            stripped_filename=builder.filenameStripper(i)
            self.assertEqual(stripped_filename, ("capture_win9"))

        name="."+os.sep+"testdata"+os.sep+"pcap"+os.sep+"2017-05-14_win10_test.pcap"
        stripped_filename=builder.filenameStripper(name)
        self.assertEqual(stripped_filename, ("2017-05-14_win10_test"))


if __name__ == '__main__':
    unittest.main()