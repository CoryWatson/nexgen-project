#!/usr/bin/env python3

import unittest
import ctu_scrape


class TestCtuScrape(unittest.TestCase):

    def test_get_links(self):
        test_list = ctu_scrape.get_links('https://www.webscraper.io/test-sites/e-commerce/allinone')

        test_list2 = ['https://forum.webscraper.io/',
                  'https://chrome.google.com/webstore/detail/web-scraper/jnhgnonknehpejjnehehllkliplmbmhn?hl=en',
                  'https://cloud.webscraper.io/',
                  'https://forum.webscraper.io/',
                  'https://www.facebook.com/webscraperio/',
                  'https://twitter.com/webscraperio']

        self.assertEqual(test_list, test_list2)

    def test_base_page_name(self):
        test_tuple = ('http://www.example.com/foo.html/',
                     'http://www.page.com/foo.html/',
                     'https://www.page.org/foo.html//')

        results = []

        for i in range(len(test_tuple)):
            results.append(ctu_scrape.base_page_name(test_tuple[i]))

        for result in results:
            self.assertEqual(result, 'foo.html')


if __name__ == '__main__':
    unittest.main()
