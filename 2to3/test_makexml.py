import unittest
import lxml
import makexml


class TestMakeXML(unittest.TestCase):
    def test_fullFilenameStripper(self):
        file_name={
        #TODO: fix this before using makexml again.
        #"../../temp/[CTU][Malware]rbot_1.pcap",
        #"/temp/[CTU][Malware]rbot_1.pcap",
        #"/[CTU][Malware]rbot_1.pcap",
        #"[CTU][Malware]rbot_1.pcap"
        }

        for i in file_name:
            stripped_filename=makexml.fullFilenameStripper(i)
            self.assertEqual(stripped_filename.name, ("rbot_1.pcap"))
            self.assertEqual(stripped_filename.category,"Malware")
            self.assertEqual(stripped_filename.source, "CTU")
