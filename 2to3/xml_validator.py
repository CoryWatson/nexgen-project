import os, sys
from lxml import etree
import argparse

##################################
# ModifiedParser class
class ModifiedParser(argparse.ArgumentParser):
  """
  Overrides argparse's ArgumentParser in order to write both the error message
  and the help message out to stderr before exiting.
  """
  def error(self, message):
    sys.stderr.write('error:%s\n' % message)
    self.print_help(sys.stderr)
    sys.exit(1)


##################################
# Validator class
class Validator:

    def __init__(self, xsd_path):
        xmlschema_doc = etree.parse(xsd_path)
        self.xmlschema = etree.XMLSchema(xmlschema_doc)

    def validate(self, xml_path):
        xml_doc = etree.parse(xml_path)
        result = self.xmlschema.validate(xml_doc)

        return result


##################################
# main stuff
if __name__ == '__main__':

    # Set up the argparser
    argparser = ModifiedParser()
    argparser.add_argument("dir", nargs='?', default="./", help="directory with xml files to validate")
    argparser.add_argument("xsd_path", nargs='?', default="./scripts/pcapData.xsd", help="location of the xsd file")

    # Parse & validate the arguments - inPath
    args = argparser.parse_args()
    dir = args.dir
    if dir[-1] != '/':
        dir += '/'
    if not os.path.exists(dir):
        print("Error: input path not found!")
        sys.exit()

    # Parse & validate the arguments - xsd_path
    xsd_path = args.xsd_path
    #if not os.path.isfile(pcapfile_name): #TODO
    if not os.path.exists(xsd_path):
        print("Error: xsd path not found!")
        sys.exit()

    validator = Validator(xsd_path)

    #print "validating xml files in directory: " + dir
    #print os.listdir(dir)

    valid_count = 0
    fail_count = 0
    for file_name in os.listdir(dir):
        if file_name[-4:] == ".xml":
            #print file_name
            file_path = '{}/{}'.format(dir, file_name)

            if validator.validate(file_path):
                valid_count += 1
                #print 'Valid: ' + file_name
            else:
                fail_count += 1
                print('Failed: ' + file_name)
        #else:
        #    print file_name + " is not an xml file, skipping ..."

    print("Total of " + str(valid_count) + " items were valid")
    print("Total of " + str(fail_count) + " items failed")
