# Nexgen Project

### This project is a copy of another git repo and is very much still a work in progress

This script is used to work with the CTU Malware Dataset to compare
the Argus Netflow with the given PCAP to extract the Botnet action packets.
The output is broken down into the UDP and TCP folders.

Implementation:

python builder.py <PCAP File> <Output Folder> <Binetflow>

Unit tests for the above script. Can be run with from this directory
with `python -m unittest discover`


ORNL PCap Numbers

PCAP								            Website #
2017-07-22_capture-win2.pcap					CTU-Malware-Capture-Botnet-286-1
2017-07-13_capture-win2.pcap					CTU-Malware-Capture-Botnet-296-1
2017-06-24_win4.pcap			    			CTU-Malware-Capture-Botnet-265-1
2017-3-29_win8.pcap				        		CTU-Malware-Capture-Botnet-240-1
2017-07-11_capture-win24.pcap					CTU-Malware-Capture-Botnet-289-1
2018-01-30_win10.pcap				    		CTU-Malware-Capture-Botnet-326-1
2017-12-18_win2.pcap				    		CTU-Malware-Capture-Botnet-320-1

Malware Capture Facility Project:
https://www.stratosphereips.org/datasets-malware
