**TODO**

* Add additional unit tests to the test_builder.py file.
  * These can use the testdata folder you added earlier.
* Threading (multiprocessing) for builder.py
  * split up the tcp\_df and udp\_df dataframes, and give each slice to a different worker?
* Resuming jobs when builder.py is interrupted
  * Periodically save its position as it iterates through the tcp\_df and udp\_df dataframes?
  * Periodically remove 'done' items from dataframes, and output a temp csv file?
  * Change arguments for new job vs resuming a job (flag arg, or optional state file arg, or etc.)?  Or automatically resume if file is found?
  * Test cases for resume feature

**Done**

* Update the readme.md file to explain what each file is/does
  * Shane did on 3/20/2019 @ 2:11pm
* Update/correct any comments in builder.py, and making sure that all methods have a comment block explaining what they do.
  * Shane did on 3/20/2019 @ 2:48pm
* Create a list of sample number vs filename, pcap size, and netflow size.
  * Personally, I would generate this with python, using the “beautiful soup” library for the scraping, but if you’d rather do it in some other way (using java instead of python, using a macro recorder, or whatever else) that’s also fine, I don’t have much of a preference.
    * ctu_scrape.py mostly complete -Cory 5/28/19
