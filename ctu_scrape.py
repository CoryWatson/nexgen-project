#!/usr/bin/env python3

"""
Scrape the CTU site and parse relevant data into a given CSV file
"""
# TODO: sort data naturally as one would expect, instead of:
# foo-1.bar
# foo-10.bar
# foo-2.bar
# foo-20.bar
import os
import sys
import argparse
from bs4 import BeautifulSoup as bs
from urllib.request import urlopen
from urllib.error import HTTPError, URLError
import urllib.parse
import ssl
import re
import time
import csv
# from datetime import datetime


def __parse_args__():
    """ Parse CLI arguments
    """
    parser = argparse.ArgumentParser(
        description='Scrape the online CTU dataset and write data to CSV file')
    parser.add_argument(
        '-d', '--html-dir', default='html' + os.sep,
        metavar='<html-directory>', help='Path to save/read scraped HTML,' +
        ' default = .' + os.sep + 'html' + os.sep)
    parser.add_argument(
        '-n', '--no-scrape', action='store_true',
        help='Don\'t scrape CTU site, only parse local HTML and write to' +
        ' CSV file')
    parser.add_argument(
        '-o', '--output-file', default='ctu_data_list.csv',
        help='Output filename, default = ctu_data_list.csv',
        metavar='<outfile>')
    parser.add_argument(
        '-t', '--sleep-time', type=float, default=1.5,
        help='Time between HTTP requests in seconds, default = 1.5',
        metavar='<seconds>')
    # the -u option is here only for debugging purposes
    parser.add_argument(
        '-u', '--url', help=argparse.SUPPRESS,
        default='https://www.stratosphereips.org/datasets-malware')
    parser.add_argument(
        '-v', '--verbose', action='store_true',
        help='Increase ouput verbosity')
    return parser.parse_args()


def get_links(url, regex='^http.*'):
    """
    Scrapes a web page for links that match a given regex
    """
    soup = get_soup(url)
    links = []
    for link in soup.find_all('a', attrs={'href': re.compile(regex)}):
        url_str = link.get('href')
        links.append(urllib.parse.unquote(url_str))

    return links


def save_html(url, file_name):
    """
    save html file locally
    """
    soup = get_soup(url)
    if type(soup) is not None:
        with open(file_name, 'w') as f:
            f.write(soup.prettify())


def get_soup(url):
    """
    read web page and return soup object
    """
    # Allows unverified SSL/TLS (https://mcfp.felk.cvut.cz has invalid cert)
    ssl._create_default_https_context = ssl._create_unverified_context

    try:
        page = urlopen(url)
    except (HTTPError, URLError) as e:
        print('get_soup() error')
        print(e + '\nURL = ' + url)
        return None

    try:
        # 'lxml' is supposed to be faster than 'html.parser'
        # 'html.parser' can be used if 'lxml' produces dependency errors
        soup = bs(page.read(), 'lxml')
    except AttributeError as e:
        print('get_soup() Attribute error')
        print(e)
        return None

    return soup


def local_get_soup(path):
    """
    read local html file and return soup object
    """
    try:
        with open(path, 'r') as f:
            soup = bs(f.read(), 'lxml')
    # TODO: get rid of bare except
    except:
        return None

    return soup


def base_page_name(url):
    """
    returns base file name of a given url
    for example: https://www.example.com/index.html/
    would return index.html
    """
    while url[-1] == '/':
        url = url[:-1]
    return url.split('/')[-1]


def write_data_to_csv(soup, csv_file):
    """
    Creates/appends data from soup object to a given CSV file
    <sample-number>, <base-filename>, <pcap-size>, <netflow-size>
    if they have multiple base file names, a new row is created
    """

    csv_exists = os.path.isfile(csv_file)
    f = open(csv_file, 'a', newline='')
    writer = csv.writer(f, lineterminator=os.linesep)

    # do we need to write the heading row?
    if not csv_exists:
        print(f'Creating new file {csv_file}')
        writer.writerow(
            ('sample number', 'base filename', 'pcap size', 'netflow size'))

    # start parsing relevant data
    if type(soup) is not None:
        sample_num = soup.title.get_text().split('/')[-1].strip()
        if args.verbose:
            print('Parsing ' + sample_num)
        table = soup.find('table')

        # get list of unique base file names
        filename_tags = table.find_all('a', attrs={
            'href': re.compile(r'\d{4}-\d{2}-\d{2}_capture.*')})
        filenames = []
        for tags in filename_tags:
            filenames.append(tags.get_text().strip())
        base_filenames = []
        for names in filenames:
            base_filenames.append(names.split('.')[0])
        base_filenames_uniq = sorted(set(base_filenames))

        # get pcap and netflow filenames
        for name in filenames:
            if re.search(r'.+.pcap$', name):
                pcap_name = name
            if re.search(r'.+.netflow$', name):
                netflow_name = name

        # start scraping relevant data
        for name in base_filenames_uniq:
            try:
                # get pcap file size
                pcap_tag = table.find('a', href=pcap_name)
                pcap_size_tag = pcap_tag.parent.parent.find(
                    'td', text=re.compile(r'^\s*[0-9]+.?[0-9]*[K|M|G]\s*$'))
                pcap_size = pcap_size_tag.get_text().strip()

                # # get netflow file size
                netflow_tag = table.find('a', href=netflow_name)
                netflow_size_tag = netflow_tag.parent.parent.find(
                    'td', text=re.compile(r'^\s*[0-9]+.?[0-9]*[K|M|G]\s*$'))
                netflow_size = netflow_size_tag.get_text().strip()

                # write data to csv file
                writer.writerow(
                    (sample_num, name, pcap_size, netflow_size))
            # TODO: get rid of bare except
            except:
                writer.writerow((sample_num, name, pcap_size, ''))
                # print(e)
                continue

    f.close()


def find_html_files(file_path):
    html_filenames = []
    try:
        if file_path[-1] != os.sep:
            file_path += os.sep
        # open path and search
        for currfile in os.listdir(file_path):
            if currfile.endswith(".html"):
                html_filenames.append(file_path + currfile)
        return html_filenames
    except FileNotFoundError:
        raise
    except NotADirectoryError:
        print(
            f'{file_path} couldn\'t be created...' +
            '\nDoes it already exist as a regular file?')
        sys.exit()


def scrape_ctu_dataset(url, sleep_time):
    ctu_links = get_links(
        url, '^.*CTU-Malware-Capture-Botnet-[0-9]+-?[0-9]*')
    ctu_links.sort()
    # print(ctu_links[-10:])

    # create 'html' dir if it doesn't already exist
    if not os.path.isdir(args.html_dir):
        os.mkdir(args.html_dir)

    # Save html for each page and append '.html' to each file
    if args.html_dir[-1] != os.sep:
        args.html_dir += os.sep
    print(
        'Saving HTML for each dataset' +
        '\nSleep time of ' + str(sleep_time) + 's between each request' +
        '\n--------------------------------------------')
    for url in ctu_links:
        html_file = args.html_dir + base_page_name(url) + '.html'
        print('Saving ' + html_file + '...')
        save_html(url, html_file)
        time.sleep(sleep_time)


def should_scrape(first_time=True):
    try:
        if first_time:
            scrape = input(
                'Do you want to scrape the CTU site for data?' +
                '\n(If you don\'t have the ' + args.html_dir +
                ' directory the answer is probably yes...)' +
                '\nScrape data? [Y/n] ')
        else:
            scrape = input('\nScrape data? [Y/n] ')
    except EOFError:
        return False

    if scrape.lower() == 'y' or scrape.lower() == 'yes':
        return True
    else:
        return False


if __name__ == '__main__':
    args = __parse_args__()

    # Do we need to scrape the CTU data?
    if not args.no_scrape:
        if should_scrape():
            scrape_ctu_dataset(args.url, args.sleep_time)

    try:
        html_files = find_html_files(args.html_dir)
    except FileNotFoundError:
        print(
            f'\nLocal {args.html_dir} directory not found!' +
            f'\nDo you want to create {args.html_dir} directory' +
            ' and scrape data from CTU site?')
        if should_scrape(False):
            scrape_ctu_dataset(args.url, args.sleep_time)
        else:
            print('Nothing left to do, exiting...')
            sys.exit()

    # how to sort html files by sample number like one would expect?
    html_files.sort()

    # If CSV file exists, delete it
    if os.path.isfile(args.output_file):
        os.remove(args.output_file)

    # Write data to CSV file
    for filename in html_files:
        soup = local_get_soup(filename)
        write_data_to_csv(soup, args.output_file)
    with open(args.output_file) as f:
        lines_written = sum(1 for _ in f)
    print(f'{lines_written} lines written to {args.output_file}')
