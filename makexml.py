#!/usr/bin/env python3
# TODO Split filename when it's passed from a folder
# TODO Count packets to map tuples to the correct XML field
# TODO Pull categories from CSV instead of coding

import sys
import os
import re
import time
import csv
import argparse

import collections
from collections import defaultdict

from scapy.all import rdpcap
from scapy.all import srp,Ether,IP,ARP,TCP,UDP,Dot1Q,conf

import attack_pcap_lib


class ModifiedParser(argparse.ArgumentParser):
    """
    Overrides argparse's ArgumentParser in order to write both the error
    message and the help message out to stderr before exiting.
    """

    def error(self, message):
        sys.stderr.write('error:%s\n' % message)
        self.print_help(sys.stderr)
        sys.exit(1)


def fullFilenameStripper(pcapFileName):
    filename = pcapFileName
    regexp = re.compile(r'\[.*\]\[.*\]')
    if regexp.search(filename):
        # print pcapFileName + ' matched'
        splitFile = filename.split("]")
        source = splitFile[0].split("[")
        source = source[-1]
        name = splitFile[1].split("[")
        name = name[-1]
        category = splitFile[2].split("[")
        category = category[-1]
    else:
        # print pcapFileName + ' didn\'t match'
        name = filename
        if os.sep in name:
            name = name.split(os.sep)[-1:][0]
        if ']' in name:
            name = name.split("]")[-1:][0]
        source = None
        category = None
    fileData = {}
    fileData["dataSource"] = source
    fileData["category"] = category
    fileData["name"] = name
    # print fileData
    return fileData


def getCategories():
    category_file = open('categories.csv')
    csv_reader = csv.reader(category_file, delimiter=',')
    categoryList = []
    categories = ["DoDLabel", "TargetLabel", "SimpleBPLabel", "AttackFrameworkLabel"]
    next(csv_reader)

    for row in csv_reader:

        if len(row) != 0:
            categoryList = categoryList + [row]

    targetLabel = categoryList[0]
    dodLabel = categoryList[1]
    basicLabel = categoryList[2]
    attackLabel = categoryList[3]
    catNames = collections.namedtuple('catNames', ['tl', 'dod', 'bsc', 'atk'])
    catList = catNames(targetLabel.pop(0), dodLabel.pop(0), basicLabel.pop(0), attackLabel.pop(0))
    Cats = collections.namedtuple('Cats', ['tl', 'dod', 'bsc', 'atk'])
    catTuple = Cats(targetLabel, dodLabel, basicLabel, attackLabel)

    # Move dictionary to the user input portion

    return catTuple, catList


def getPcapInfo(pktList):
    cap = pktList
    packetAmount = len(cap)
    # Find source and destination packets
    # Loop through the packets and count the instances each unique IP appears
    srcDict = {}
    dstDict = {}
    sportDict = {}
    dportDict = {}
    for packet in cap:
        # Initializing next two vars to empty string because they were
        # getting referenced before assignment (UnboundLocalError)
        currDport = ''
        currSport = ''
        if 'TCP' in packet:
            currDport = str(packet[TCP].dport)
        elif 'UDP' in packet:
            currDport = str(packet[UDP].dport)
        if not currDport in dportDict:
            dportDict[currDport] = 1
        else:
            dportDict[currDport] += 1
        if 'TCP' in packet:
            currSport = str(packet[TCP].sport)
        elif 'UDP' in packet:
            currSport = str(packet[UDP].sport)
        if not currSport in sportDict:
            sportDict[currSport] = 1
        else:
            sportDict[currSport] += 1
        if not str(packet[IP].dst) in dstDict:
            dstDict[str(packet[IP].dst)] = 1
        else:
            dstDict[str(packet[IP].dst)] += 1
        if not str(packet[IP].src) in srcDict:
            srcDict[str(packet[IP].src)] = 1
        else:
            srcDict[str(packet[IP].src)] += 1

    srcIP = max(iter(srcDict.keys()), key=(lambda key: srcDict[key]))
    destIP = max(iter(dstDict.keys()), key=(lambda key: dstDict[key]))
    srcPort = max(iter(sportDict.keys()), key=(lambda key: sportDict[key]))
    destPort = max(iter(dportDict.keys()), key=(lambda key: dportDict[key]))
    print(("Discovered- SourceIP: " + srcIP, "DestIP: "
           + destIP, "SourcePort: " + srcPort, "DestPort: " + destPort))
    firstPkt = cap[0]
    lastPkt = cap[packetAmount-1]
    startTime = float(firstPkt.time)
    endTime = float(lastPkt.time)
    totalTime = round(endTime - startTime, 3)
    pcapData = collections.namedtuple('pcapData', ['destIP', 'srcIP', 'destPort', 'srcPort', 'totalTime', 'startTime','endTime', 'packetAmount'])
    pcapTuple = pcapData(destIP, srcIP, destPort, srcPort, totalTime, startTime, endTime, packetAmount)
    return pcapTuple


def buildXml(inputFileName, outPath):
    print("processing file: " + inputFileName)
    fileData = fullFilenameStripper(inputFileName)
    catTuple, catList = getCategories()
    pktList = rdpcap(inputFileName)
    pcapTuple = getPcapInfo(pktList)
    cerNum = ""
    incNum = ""

    # Run the questionairre for user input

    # TODO: handle cases where file may not have a source specified
    # in [][]name style.

    if fileData["dataSource"]:
        dataSource = fileData["dataSource"]
    else:
        dataSource = input("Pcap data Source: ")
        dataSource = attack_pcap_lib.make_clean_file_name_string(dataSource)
    # print "using source of: " + dataSource

    dodDict = {(int(key) + 1): value.strip() for key, value in enumerate(catTuple.dod)}
    basicDict = {(int(key) + 1): value.strip() for key, value in enumerate(catTuple.bsc)}
    targetDict = {(int(key) + 1): value.strip() for key, value in enumerate(catTuple.tl)}
    attackDict = {(int(key) + 1): value.strip() for key, value in enumerate(catTuple.atk)}
    catDict = {(int(key) + 1): value.strip() for key, value in enumerate(catList)}

    for key, value in targetDict.items():
        print(key, value)
    key = eval(input("Choose the number which matches the most correct "
                     "Targetlabel: "))
    tlItem = targetDict.get(key)

    for key, value in dodDict.items():
        print(key, value)
    key = eval(input("Choose the number which matches the most correct "
                     "DoDLabel: "))
    dodItem = dodDict.get(key)

    for key, value in basicDict.items():
        print(key, value)
    key = eval(input("Choose the number which matches the most correct "
                     "SimpleBPLabel: "))
    bscItem = basicDict.get(key)

    for key, value in attackDict.items():
        print(key, value)
    key = eval(input("Choose the number which matches the most correct "
                     "AttackFrameworkLabel: "))
    attackItem = attackDict.get(key)

    for key, value in catDict.items():
        print(key, value)
    key = eval(input("Which label should be used for defining this attack? "))
    getCat = catDict.get(key)
    if getCat == "DoDLabel":
        catValue = dodItem
    elif getCat == "TargetLabel":
        catValue = tlItem
    elif getCat == "AttackFrameworkLabel":
        catValue = attackItem
    else:
        catValue = bscItem

    description = input("Give a brief description of malware's activities: ")

    success = False
    successText = input("Was the attack successful? ")

    # 'is' tests for identity not equality
    # i.e. '('a' * 10) is ('a' * 10)' returns False
    # do we really want 'is' here?
    # if successText.lower() is "yes" or "y":
    if successText.lower() == "yes" or "y":
        success = True
    else:
        success = False
    cerField = False
    cer = input("Is there a CER for this file? y/n: ")
    if cer.strip() == "y":
        cerNum = input("Enter CER number: ")
    incident = input("Is there an Incident number for this file? y/n: ")
    if incident.strip() == "y":
        incNum = input("Enter Incident number: ")

    catValueClean = attack_pcap_lib.make_clean_file_name_string(catValue)

    # '/' and '\\' are already included in attack_pcap_lib.make_clean_file_name_string()
    # catValueClean = catValueClean.replace('/', "_")
    # catValueClean = catValueClean.replace('\\', "_")
    # print dataSource
    # print catValueClean
    # print fileData["name"]
    outPcapFileName = "[" + dataSource + "][" + catValueClean + "]" + fileData["name"]
    outXmlFileName = outPcapFileName.replace("pcap","xml")

    # Populate the strike info obj, to pass to write_output_files()
    strike_info = {}
    strike_info["Strike Name"] = description
    strike_info["Category"] = catValue
    cats = {}
    cats["SimpleBPLabel"] = bscItem
    cats["TargetLabel"] = tlItem
    cats["DoDLabel"] = dodItem
    cats["AttackFrameworkLabel"] = attackItem
    strike_info["Categories"] = cats
    strike_info["Source IP"] = str(pcapTuple.srcIP)
    strike_info["Destination IP"] = str(pcapTuple.destIP)
    strike_info["Source Port"] = str(pcapTuple.srcPort)
    strike_info["Destination Port"] = str(pcapTuple.destPort)
    strike_info["Protocol"] = "tcp"
    strike_info["success"] = str(success)
    strike_info["NCD"] = incNum
    strike_info["CER"] = cerNum
    strike_info["Source File"] = inputFileName
    strike_info["Data Source"] = dataSource

    attack_pcap_lib.write_output_files(strike_info, pktList, outPath, outPcapFileName.replace(".pcap", ""))


if __name__ == '__main__':

    # Set up the argparser
    argparser = ModifiedParser(description="Generate XML from PCAP files")
    argparser.add_argument("inPath", nargs='?', default="." + os.sep
                           + "testdata" + os.sep + "makexml", help="Input dir")
    argparser.add_argument("outPath", nargs='?', default="." + os.sep
                           + "testdata" + os.sep + "makexml" + os.sep
                           + "output", help="Output dir")

    # Parse & validate the arguments - inPath
    args = argparser.parse_args()
    in_path = args.inPath
    if in_path[-1] != os.sep:
        in_path += os.sep

    if not os.path.exists(in_path):
        print("Error: input path not found!")
        sys.exit()

    # Parse & validate the arguments - outPath
    out_path = args.outPath
    if out_path[-1] != os.sep:
        out_path += os.sep

    if out_path == in_path:
        print("Error: output path must be different from input path!")
        sys.exit()

    if not os.path.exists(out_path):
        os.makedir(out_path)

    pcap_file_names = attack_pcap_lib.find_pcap_files(in_path)

    for pcap_file_name in pcap_file_names:
        buildXml(pcap_file_name, out_path)
