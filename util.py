#Utility functions, for parsing and etc.

import os
import csv
import re
import sys
#import urllib.request, urllib.parse, urllib.error
import argparse
import datetime
import functools
import warnings
from functools import partial
from pprint import pprint

import pandas as pd
from pandas import DataFrame, read_csv

class ModifiedParser(argparse.ArgumentParser):
    """
    Overrides argparse's ArgumentParser in order to write both the error message
    and the help message out to stderr before exiting.
    """
    def error(self, message):
        sys.stderr.write('error:%s\n' % message)
        self.print_help(sys.stderr)
        sys.exit(1)

class Flow:
    def __init__(self):
        #time info
        self._startTime = None #datetime.datetime obj
        #self.startTimeOfDay
        #self.startDayOfWeek
        self.duration = 0.0
        #src/dest info
        self.proto = "tcp"
        self.srcIP = "0.0.0.0"
        self.srcPort = 0
        self.dstIP = "0.0.0.0"
        self.dstPort = 0
        #src/dest location & geo info
        self.srcCountry = ""
        self.srcASNum = 0
        self.dstCountry = ""
        self.dstASNum = 0
        self.isSrcLocal = False
        self.isDstLocal = False
        #volume info
        self.srcBytes = 0
        self.srcPkts = 0
        self.dstBytes = 0
        self.dstPkts = 0
        self.flowPcr = 1.0

    def setStartTime(self, time_str, time_format):
        self._startTime = datetime.strptime(time_str, time_format)
    @property
    def startTimeOfDay(self): #in hours
        hours = self._startTime.hour
        hours += (1.0 + self._startTime.minute) / 60.0
        hours += ((1.0 + self._startTime.second) / 60.0) / 60.0
        #truncate any microseconds
        return hours
    @property
    def startDayOfWeek(self): #sun: 0, mon: 1, etc.
        return self._startTime.weekday()

def parse_argus_csv_file(inFile):
    flow_list = []
    ###typical headers include these:
    #Proto	SrcId	StartTime	LastTime	Dur
    #SrcAddr	Dir	DstAddr	Sport	Dport
    #TotPkts	TotBytes	TotAppByte	Flgs
    #SAppBytes	SrcBytes	SrcPkts	DAppBytes	DstBytes	DstPkts
    #sCo	dCo	sAS	dAS	PCRatio	Label
    #headers = open(inFile).readline()
    #headers = headers.strip().replace(" ", "").split(',')
    #headers_to_use = headers
    #headers_to_use = [x for x in headers_to_use if x != "Label"] # omit the "Label" field: not useful, and hard to parse.
    #headers_to_use = [x for x in headers_to_use if x != "StartTime" and x != "LastTime"] # also skip un-parsed timestamps
    #headers_to_use = [x for x in headers_to_use if x != "SrcAddr" and x != "DstAddr"] # also skip raw addresses
    #headers_to_use = [x for x in headers_to_use if x != "sAS" and x != "dAS"] # also skip raw addresses
    #headers_to_use = [x for x in headers_to_use if x != "SrcId"] # also these
    xl = ['StartTime', 'Dur', 'Proto', 'SrcAddr', 'Sport', 'Dir', 'DstAddr',
       'Dport', 'TotPkts', 'TotBytes', 'Label']
    print(xl)
    print()
    df = pd.read_csv(inFile,dtype=str,usecols=xl)

    return df

def parse_situ_json_file(inFile):
    #flow_list = []
    #TODO parse
    #return flow_list
    return None

def parse_source_args():
    # Set up the argparser
    argparser = ModifiedParser()
    argparser.add_argument("inFile", nargs='?', help="Input dir",
            default="./testdata/bldg6012.10k.csv")
    argparser.add_argument("inFileFormat", nargs='?',
            default="argus-csv",
            help="format of input file.  currently supported: argus-csv, situ-json")

    # Parse argument, validate the inFile argument
    args = argparser.parse_args()
    inFile = args.inFile
    if not os.path.isfile(inFile):
        print("Error: input file not found!")
        sys.exit()

    # validate the 'inFileFormat' arg, and invoke needed parsing function.
    inFileFormat = args.inFileFormat
    #TODO: support for CTU, ISCX, etc.

    # inFile = "./testdata/bldg6012.10k.csv"
    # inFileFormat = "argus-csv"

    return inFile, inFileFormat

def parse_source_file(inFile, inFileFormat):
    print("parsing file", inFile)
    print("as format", inFileFormat)
    if inFileFormat == "situ-json":
        print("Unsupported file format: " + inFileFormat)
        #TODO: implement this later
    if inFileFormat == "argus-csv":
        flow_frame = parse_argus_csv_file(inFile)
    else:
        print("Unknown file format: " + inFileFormat)
        sys.exit()
    return flow_frame
